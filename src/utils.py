import os
import shutil
import traceback
import configparser

from logger import Logger

from pyspark import SparkContext, SparkConf
from pyspark.sql.session import SparkSession

SHOW_LOG = True

def rm_dir(path):
    if os.path.exists(path):
        if os.path.isdir(path):
            shutil.rmtree(path)
        else:
            os.remove(path)
    return os.path.exists(path)

class SparkAdapter():

    def __init__(self):

        self.config = configparser.ConfigParser()
        self.log = Logger(SHOW_LOG).get_logger(__name__)
        self.config_path = os.path.join(os.getcwd(), 'config.ini')
        self.config.read(self.config_path)
        
        self.spark_config = SparkConf()
        self.spark_config.set("spark.app.name", "spark-rec")
        self.spark_config.set("spark.master", "local")
        self.spark_config.set("spark.executor.cores", \
            self.config.get("SPARK", "NUM_PROCESSORS", fallback="3"))
        self.spark_config.set("spark.executor.instances", \
            self.config.get("SPARK", "NUM_EXECUTORS", fallback="1"))
        self.spark_config.set("spark.executor.memory", "16g")
        self.spark_config.set("spark.locality.wait", "0")
        self.spark_config.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
        self.spark_config.set("spark.kryoserializer.buffer.max", "2000")
        self.spark_config.set("spark.executor.heartbeatInterval", "6000s")
        self.spark_config.set("spark.network.timeout", "10000000s")
        self.spark_config.set("spark.shuffle.spill", "true")
        self.spark_config.set("spark.driver.memory", "16g")
        self.spark_config.set("spark.driver.maxResultSize", "16g")

        self.num_parts = self.config.getint("SPARK", "NUM_PARTS", fallback=None)
        self.sc = None
        self.log.info("Spark Initialized")

    def get_context(self):
        self.sc = SparkContext(conf=self.spark_config)
        return self.sc
    
    def get_session(self):
        if not self.sc:
            return SparkSession(self.get_context())
        else:
            return SparkSession(self.sc)