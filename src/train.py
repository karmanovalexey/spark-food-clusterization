import os
import pandas as pd
import traceback
import configparser

from pyspark.ml.feature import VectorAssembler
from pyspark.mllib.linalg import Vectors
from pyspark.mllib.linalg.distributed import IndexedRow, IndexedRowMatrix
from pyspark.mllib.linalg.distributed import MatrixEntry, CoordinateMatrix
from pyspark.ml.clustering import KMeans
from pyspark.sql.types import StructField, FloatType, StructType

from utils import SparkAdapter
from unload_db import DBhandler

from logger import Logger
from utils import rm_dir, SparkAdapter

SHOW_LOG = True

class Trainer():
    def __init__(self):
        """
        Initialize logger
        """
        self.log = Logger(SHOW_LOG).get_logger(__name__)
        self.log.info("Trainer Initialized")
        
    def load_data(self):
        """
        Load first one third of data rows for convenience to pandas DataFrame 
        """
        self.log.info(f'Loading data')
        db_hand = DBhandler('food_in')
        self.full_data = db_hand.dump_to_np()
        if self.full_data.shape[0] > 100000:
            self.full_data = self.full_data[:100000]
        self.pd_df = pd.DataFrame(self.full_data)
        self.log.info(f'Got data shaped {self.full_data.shape}')
        self.schema = StructType([StructField("1", FloatType(), True),
                            StructField("2", FloatType(), True),
                            StructField("3", FloatType(), True),
                            StructField("4", FloatType(), True),
                            StructField("5", FloatType(), True),
                            StructField("6", FloatType(), True),
                            StructField("7", FloatType(), True),
                            StructField("8", FloatType(), True),
                            StructField("9", FloatType(), True),
                            StructField("10", FloatType(), True),
                            StructField("11", FloatType(), True),
                            StructField("12", FloatType(), True),])

    def load_data_csv(self):
        """
        Load rows to pandas DataFrame 
        """
        self.log.info(f'Loading data from csv')
        self.pd_df = pd.read_csv('cache/dm_tmp.csv')
        self.log.info(f'Got data {self.pd_df.head}')
        self.schema = StructType([StructField("1", FloatType(), True),
                            StructField("2", FloatType(), True),
                            StructField("3", FloatType(), True),
                            StructField("4", FloatType(), True),
                            StructField("5", FloatType(), True),
                            StructField("6", FloatType(), True),
                            StructField("7", FloatType(), True),
                            StructField("8", FloatType(), True),
                            StructField("9", FloatType(), True),
                            StructField("10", FloatType(), True),
                            StructField("11", FloatType(), True),
                            StructField("12", FloatType(), True),])
        

    def to_matrix(self, data):
        """
        Assemble Data to Feature Vector to process 
        """
        vecAssembler = VectorAssembler(inputCols=["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"], outputCol="features")
        new_df = vecAssembler.transform(data)
        return new_df

    def unload_data(self, data, with_prediction=False):
        """
        Write Data to DataBase
        """
        self.log.info(f'Writing data')
        db_hand = DBhandler('food_clustered')
        self.full_data = db_hand.dump_to_db(data, with_prediction)

    def train(self):
        """
        Looad data
        Convert to desired types
        Configure KMeans
        Run it
        Load data back to db
        """
        try:
            adapter = SparkAdapter()
            context = adapter.get_context()
            session = adapter.get_session()
        except:
            self.log.error(traceback.format_exc())
            return False

        self.load_data()

        # create DF
        spark_df = session.createDataFrame(self.pd_df, schema=self.schema)
        
        # Mapped to Matrix
        self.log.info('Data To Matrix')
        new_df = self.to_matrix(spark_df)

        self.log.info('KMeans training')
        kmeans_val = KMeans(k=4, seed=42, featuresCol='features', predictionCol='prediction')
        model = kmeans_val.fit(new_df)
        pred = model.summary.predictions.toPandas()

        pred = pred.drop('features', axis=1)
        val_count = pred['prediction'].value_counts()
        self.log.info(f'Clusters:\n {val_count}')

        self.unload_data(pred.to_numpy(), with_prediction=True)

if __name__ == "__main__":
    trainer = Trainer()
    trainer.train()