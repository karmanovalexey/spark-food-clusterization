// spark-shell

object DFFromCSV{
    def main(args: Array[String]): Unit = {
        val df = spark.read.options(Map("delimiter" -> "\t", "header" -> "false")).csv("cache/tmp.csv")
        // As Data is already clean, we take first 100000 pieces 
        val data = df.take(100000)
        data.write.options(Map("delimiter" -> "\t", "header" -> "false")).csv("cache/dm_tmp.csv")
    }
}

DFFromCSV.main(Array("Datamart"))
