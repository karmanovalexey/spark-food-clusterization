# Clusterization pipeline using HBase
## Installation
Installed HBase, Haddop and Spark following this tutorial :

https://kontext.tech/thread/628/spark-connect-to-hbase

Didn't use docker because of hbase setup issues

## Load data to DB
Loaded data to HBase following this:

https://docs.cloudera.com/cdsw/1.9.2/import-data/topics/cdsw-load-data-into-hbase-table.html

## Get data
### Directly from HBase to pyspark
Used happybase to load out data from DB to pyspark. After this, converted to pyspark DataFrame

### Using Datamart
Using DBhandler class and spark scala script to filter data and convert it to csv

## KMeans

Ran Kmeans in Pyspark getting 4 clusters

![](report/clusters.png)

## Load back

Load back data with predictions to HBase using happybase

![](report/written.png)